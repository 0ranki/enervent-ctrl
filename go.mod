module github.com/0ranki/enervent-ctrl

go 1.18

require (
	github.com/0ranki/https-go v0.0.0-20230314064508-ba9a558db433
	github.com/goburrow/modbus v0.1.0
	github.com/gorilla/handlers v1.5.1
	github.com/prometheus/client_golang v1.14.0
	gopkg.in/yaml.v3 v3.0.1
)

require golang.org/x/xerrors v0.0.0-20220907171357-04be3eba64a2 // indirect

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/goburrow/serial v0.1.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/prometheus/client_model v0.3.0 // indirect
	github.com/prometheus/common v0.37.0 // indirect
	github.com/prometheus/procfs v0.8.0 // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
